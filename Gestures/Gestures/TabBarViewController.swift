//
//  TabBarViewController.swift
//  Gestures
//
//  Created by Santiago Pazmiño on 26/1/18.
//  Copyright © 2018 Santiago Pazmiño. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        let items = tabBar.items!
        
        items[0].title = "Tab View"
        items[0].image = #imageLiteral(resourceName: "ic_fingerprint")
        
        items[1].title = "Tab CircleView"
        items[1].image = #imageLiteral(resourceName: "ic_check_circle")
    }
}
