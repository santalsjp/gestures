//
//  TapViewController.swift
//  Gestures
//
//  Created by Santiago Pazmiño on 22/1/18.
//  Copyright © 2018 Santiago Pazmiño. All rights reserved.
//

import UIKit

class TapViewController: UIViewController {
    
    @IBOutlet weak var taps: UILabel!
    
    
    @IBOutlet weak var touchesLabel: UILabel!
    
    
    @IBOutlet weak var viewFrame: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touchesCount = touches.count
        let touch = touches.first
        let tapCount = touch!.tapCount
        if viewFrame.frame.contains(touch!.location(in: self.view)){
            print("Alerta")
        }else{
            touchesLabel.text = "\(touchesCount)"
             taps.text = "\(tapCount)"
        }
       
        
       
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touche = touches.first
        let location = touche?.location(in: view)
        
        let x = location?.x
        let y = location?.y
        
        print("x: \(x ?? 0) y: \(y ?? 0)")
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        taps.text = "0"
        touchesLabel.text = "0"
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("cancelado")
    }

}
