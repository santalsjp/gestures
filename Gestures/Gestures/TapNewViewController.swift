//
//  TapNewViewController.swift
//  Gestures
//
//  Created by Santiago Pazmiño on 22/1/18.
//  Copyright © 2018 Santiago Pazmiño. All rights reserved.
//

import UIKit

class TapNewViewController: UIViewController {

    @IBOutlet weak var viewFrame: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

   
    @IBAction func tapGestures(_ sender: Any) {
      
              viewFrame.backgroundColor = .red
        
    }
    
    @IBAction func swipeUpAction(_ sender: Any) {
        viewFrame.backgroundColor = .yellow
    }
    
    @IBAction func zoomAction(_ gestureRecognizer : UIPinchGestureRecognizer) {   guard gestureRecognizer.view != nil else { return }
        gestureRecognizer.view?.transform = (gestureRecognizer.view?.transform.scaledBy(x: gestureRecognizer.scale, y: gestureRecognizer.scale))!
        if gestureRecognizer.state == .ended
        {
            
            viewFrame.frame.size.height = 128
            viewFrame.frame.size.width = 45
            viewFrame.center = self.view.center
            gestureRecognizer.scale = 1.0
          
           
        }else{
            
        }
      
    }
}
